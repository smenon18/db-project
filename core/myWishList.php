<?php
    include "../templates/master.html";
?>
    <div class="container-fluid" align="center">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h1>Your Wish List</h1>
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-12 col-lg-12">
                <table id="wishlist" class="table">
                    <tr>
                        <th>UPC</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <script src="../js/getWishlist.js"></script>
    
    </body>
</html>

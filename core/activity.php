<?php
    include "../templates/adminMaster.html";
?>
        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Underactive Customers</h1>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <table id="customers" class="table">
                        <tr>
                            <th>Customer ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        <script src="../js/getActivity.js"></script>
        
    </body>
</html>
<!--
    Admin Login panel to access admin features.
    
    @author Richard Cerone
    @date 4/10/2016
-->

<?php
    include "../templates/master.html";
?>

<!-- Main Content -->
    
        <div class="container-fluid" align="center">
            
            <!-- Title -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Admin Login</h1>
                </div>
            </div>
            <br>
            
            <!-- Username -->
            <div class="row">
               <div class="col-md-12 col-lg-12">
                   <h4>Admin Username:</h4>
               </div>
            </div>
            
            <div class="row">
               <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                   <input type="text" id="adminName" class="form-control" placeholder="Admin Username"/>
               </div>
            </div>
            
            <!-- Password -->
            <div class="row">
               <div class="col-md-12 col-lg-12">
                   <h4>Admin Password:</h4>
               </div>
            </div>
            
            <div class="row">
               <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                   <input type="password" id="adminPassword" class="form-control" placeholder="Admin Password"/>
               </div>
            </div>
            <br>
            
            <!-- Login -->
            <div class="row">
               <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                   <button id="adminLogin" class="form-control btn btn-success">Login</button>
               </div>
            </div>
            
        </div>
        
        <script src="../js/adminLogin.js"></script>
    
    </body>
</html>
<?php
    include "../templates/adminMaster.html";
?>

        <div class="container-fluid" align="center">
            
            <!-- Greeting -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1 id="greeting">Welcome Master</h1>
                </div>
            </div>
            <br>
                
            <!-- Checkout -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="inventory" class="form-control btn btn-success">Inventory</button>
                </div>
            </div>
            <br>
            
            <!-- Track Orders -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="activity" class="form-control btn btn-success">Customer Activity</button>
                </div>
            </div>
            <br>
            
            <!-- Wishlist -->
              <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="wishlist" class="form-control btn btn-success">Wishlists</button>
                </div>
            </div>
            
            <script src="../js/adminRedirects.js"></script>
            
        </div>
    </body>
</html>
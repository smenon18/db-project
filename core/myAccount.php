<!--
    Allows customer to view cart, checkout, modify wishlist, and track orders.
    
    @author Richard Cerone
    @date 4/10/2016
-->

<?php
    include "../templates/master.html";
?>

<!-- Main Content -->
        <!-- Make sure user is logged in. -->
        <script src="../js/checkLoggedIn.js"></script>
        <div class="container-fluid" align="center">
            
            <!-- Greeting -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1 id="greeting">Welcome</h1>
                </div>
            </div>
            <br>
                
            <!-- Checkout -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="checkout" class="form-control btn btn-success">Checkout</button>
                </div>
            </div>
            <br>
            
            <!-- Track Orders -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="track" class="form-control btn btn-success">Track Orders</button>
                </div>
            </div>
            <br>
            
            <!-- Wishlist -->
              <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="wishlist" class="form-control btn btn-success">My Wishlist</button>
                </div>
            </div>
            <br>
            
        </div>
        
        <script src="../js/loadGreeting.js"></script>
        <script src="../js/myAccountRedirects.js"></script>
    </body>
</html>
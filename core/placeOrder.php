<?php
    include "../templates/master.html";
?>
        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Enter Payment Information</h1>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Card Type</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <select id="card" class="form-control">
                        <option value="DISCOVER">Discover</option>
                        <option value="MC">Master Card</option>
                        <option value="VISA">Visa</option>
                    </select>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Card Number</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" id="cardNumber" class="form-control" placeholder="Card Number"/>
                </div>
            </div>
            <br>
            
             <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="order" class="btn btn-success form-control">Place Order</button>
                </div>
            </div>
             <br>
             
             <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="cancel" class="btn btn-danger form-control">Cancel</button>
                </div>
            </div>
            
        </div>
        
        <script src="../js/placeOrder.js"></script>
        <script src="../js/cancelOrder.js"></script>
    </body>
</html>


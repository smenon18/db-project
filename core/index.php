<!--
    Main homepage for login/signup
    
    @author Richard Cerone
    @date 4/10/2016
-->

<?php
    include "../templates/master.html";
?>

<!-- Main content -->
        <script src="../js/loggedIn.js"></script>
        <div class="container-fluid" align="center">
            
            <!-- Title -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Welcome to My Fancy Retailer!</h1>
                </div>
            </div>
            
            <!-- Customer Login -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Are You A Customer? Login!</h4>
                </div>
            </div>
            <br>
            
            <!-- Username -->
            <div class="row">        
                <div class="col-md-12 col-lg-12">
                    <h4>Username:</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="username" placeholder="Username"/>  
                </div>
            </div>
            <br>
            
            <!-- Password -->
            <div class="row">        
                <div class="col-md-12 col-lg-12">
                    <h4>Password:</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="password" class="form-control" id="password" placeholder="Password"/>
                </div>
            </div>
            <br>
            
            <!-- Login Button -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="login" class="form-control btn btn-success">Login</button>
                </div>
            </div>
            <br>
            
            <!-- Signup Link -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <a href="signup.php">New customer? Signup here!</a>
                </div>
            </div>
            
        </div>
        
        <script src="../js/customerLogin.js"></script>
    </body>
</html>

<?php
    include "../templates/master.html";
?>
        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1 id="name"></h1>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4 id="supplier"></h4>
                </div>
            </div>
            <br><br><br><br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4 id="price"></h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div id="avgRating"><b>Average Rating:</b></div>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Quantity</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-2 col-lg-2 col-md-offset-5 col-lg-offset-5">
                   <select id="amount" class="form-control">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                   </select>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-1 col-lg-1 col-md-offset-5 col-lg-offset-5">
                    <button id="add" class="btn btn-success">Add to Cart</button>
                </div>
                
                 <div class="col-md-1 col-lg-1">
                    <button id="wishlist" class="btn btn-success">Add to Wishlist</button>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Rate This Product</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-2 col-lg-2 col-md-offset-5 col-lg-offset-5">
                   <select id="score" class="form-control">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                   </select>
                </div>
            </div>
            <br>
            
            <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                <button id="rate" class="btn btn-success">Rate</button>
            </div>
            <br><br><br><br>
            
            <div class="row">
                <div id="status" class="col-md-12 col-lg-12">
                    <table id="ratings" class="table">
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Rating</th>
                            <th>Rating Date</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        <script src="../js/getURLParam.js"></script>
        <script src="../js/getProductInfo.js"></script>
        <script src="../js/getRatings.js"></script>
        <script src="../js/averageRating.js"></script>
        <script src="../js/addToCart.js"></script>
        <script src="../js/rate.js"></script>
        <script src="../js/addToWishList.js"></script>
        
    </body>
</html>
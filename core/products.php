<!--
    Products page to view all products

    @author Shaan Menon
    @date 4/10/2016
-->

<?php
    include "../templates/master.html";
?>

<!-- Begin Content -->
    <div class="container-fluid" align="center">

        <!-- Title -->
	<div class="row">
	    <div class="col-md-12 col-lg-12">
	        <h1>Product List</h1>
	    </div>
	</div>

	<!-- Begin Suggested Products -->
	<div class="row">
		<div id="suggested"></div>
	    <div class="col-md-12 col-lg-12">
		<table id="suggested-product-table" class="table">
		</table>
	    </div>
	</div>

	<!-- Begin Products -->
	<div class="row">
	    <div class="col-md-12 col-lg-12">
	        <table id="product-table" class="table">
		    <tr>
			<th id="name">Product Name</th>
			<th id="supplier">Supplier</th>
			<th id="price">Price</th>
		    </tr>
		</table>
	    </div>
	</div>
	
	<script src="../js/getRecommendedProducts.js"></script>
	<script src="../js/getProducts.js"></script>
	
    </body>
</html>

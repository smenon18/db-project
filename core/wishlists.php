<?php
    include "../templates/adminMaster.html";
?>
        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1 id="greeting">Most Wished For Items</h1>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <table id="items" class="table">
                        <tr>
                            <th>UPC</th>
                            <th>Name</th>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>
            
        <script src="../js/getMostWishedFor.js"></script>
    </body>
</html>
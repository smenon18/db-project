<?php
    include "../templates/master.html";
?>
    <div class="container-fluid" align="center">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h1>Track Orders</h1>
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-12 col-lg-12">
                <table id="orders" class="table">
                    <tr>
                        <th>Order ID</th>
                        <th>Date Ordered</th>
                        <th>Date Shipped</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <script src="../js/loadTrackOrders.js"></script>

    </body>
</html>

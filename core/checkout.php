<?php
    include "../templates/master.html";
?>

        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Your Cart</h1>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <table id="cart" class="table">
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Amount</th>
                        </tr>
                    </table>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4 id="total" align="right">total:</h1>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="checkout" class="form-control btn btn-success">Checkout</button>
                </div>
            </div>
            <br>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="clear" class="form-control btn btn-danger">Clear Cart</button>
                </div>
            </div>
            
        </div>
        
        <script src="../js/loadCart.js"></script>
        <script src="../js/checkout.js"></script>
        <script src="../js/clearCart.js"></script>
    </body>
</html>
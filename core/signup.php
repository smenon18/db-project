<?php
    include "../templates/master.html";
?>      
        <div class="container-fluid" align="center">
            
            <!-- Title -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Signup</h1>
                </div>
            </div>
            
            <!-- First Name -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>First Name</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="firstName" placeholder="First Name"/>
                </<div>
            </div>
            
            <!-- Last Name -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Last Name</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="lastName" placeholder="Last Name"/>
                </<div>
            </div>
                
            <!-- Username -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Username</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="username" placeholder="Username"/>
                </<div>
            </div>
            
            <!-- Password -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Password</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="password" class="form-control" id="password" placeholder="Password"/>
                </<div>
            </div>
                
            <!-- State -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>State</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <select id="state" class="form-control">
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Conneticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FA">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
                </<div>
            </div>
            
            <!-- Address -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Address</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="street" placeholder="Address"/>
                </<div>
            </div>
                
            <!-- City -->
             <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>City</h4>
                </div>
            </div>
                
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="city" placeholder="City"/>
                </<div>
            </div>    
                
            <!-- Zip -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h4>Zip Code</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <input type="text" class="form-control" id="zip" placeholder="Zip Code"/>
                </<div>
            </div>
            <br><br><br> 
                
            <!-- Signup -->
            <div class="row">
                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <button id="signup" class="form-control btn btn-success">Signup</button>
                </<div>
            </div>
        </div>
                
        <script src="../js/signup.js"></script>
    </body>
</html>
<?php
    include "../templates/adminMaster.html";
?>
        <div class="container-fluid" align="center">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1>Reorder Inventory</h1>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <table id="inventory" class="table">
                        <tr>
                            <th>UPC</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Supplier</th>
                            <th>Amount</th>
                            <th>Reorder Level</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        <script src="../js/getReorder.js"></script>
        
    </body>
</html>
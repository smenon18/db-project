<?php
    require "connect.php";

    $upc = $_POST["upc"];
    $sql = "SELECT name, sname, price  FROM product WHERE upc=" . $upc;
    
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $product = array();
        array_push($product, $row[0]["name"]);
        array_push($product, $row[0]["sname"]);
        array_push($product, $row[0]["price"]);
        
        echo json_encode($product);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
<?php
    require "connect.php";
    
    $cid = $_POST["cid"];
    $sql = "CALL trackOrder('$cid')";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $trackers = array();
        for($i = 0; $i < sizeof($row); $i++)
        {
            $tracker = new stdClass;
            $tracker->orderID = $row[$i]["oid"];
            $tracker->dateOrdered = $row[$i]["date_ordered"];
            if($row[$i]["date_shipped"] === null)
            {
                $tracker->dateShipped = "";
            }
            else
            {
                $tracker->dateShipped = $row[$i]["date_shipped"];
            }
            
            array_push($trackers, $tracker);
        }
        
        echo json_encode($trackers);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
<?php
    require "connect.php";
    
    $sql = "CALL getWishedFor()";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $items = array();
        
        for($i = 0; $i < sizeof($row); $i++)
        {
            $wish = new stdClass;
            $wish->upc = $row[$i]["upc"];
            $wish->name = $row[$i]["name"];
            
            array_push($items, $wish);
        }
        echo json_encode($items);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
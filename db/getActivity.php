<?php
    require "connect.php";
    
    $sql = "call underactive_custs()";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $customers = array();
        for($i = 0; $i < sizeof($row); $i++)
        {
            $customer = new stdClass;
            $customer->cid = $row[$i]["cid"];
            $customer->first = $row[$i]["fname"];
            $customer->last = $row[$i]["lname"];
            
            array_push($customers, $customer);
        }
        echo json_encode($customers);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
<?php
    require "connect.php";
    
    $sql = "CALL getReOrderProducts()";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $items = array();
        for($i = 0; $i < sizeof($row); $i++)
        {
            $product = new stdClass;
            $product->upc = $row[$i]["upc"];
            $product->name = $row[$i]["name"];
            $product->price = $row[$i]["price"];
            $product->sname = $row[$i]["sname"];
            $product->amount = $row[$i]["amount"];
            $product->reorderlevel = $row[$i]["reorderlevel"];
            
            array_push($items, $product);
        }
        
        echo json_encode($items);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
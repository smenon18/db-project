<?php
    require "connect.php";
    
    $products = array();
    $emptyString = "";
    
    $sql = "CALL view_products(1)";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        
        for($i = 0; $i < sizeof($row); $i++)
        {
            $product = new stdClass;
            $product->upc = $row[$i]["upc"];
            $product->name = $row[$i]["name"];
            $product->price = $row[$i]["price"];
            $product->supplier = $row[$i]["sname"];
            
            array_push($products, $product);
        }
        echo json_encode($products);
        
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
-- Stored Procudures for db project
-- Author: Shaan Menon
-- Date: 10th April 2016

CREATE FUNCTION SPLIT_STR(x VARCHAR(255),delim VARCHAR(12),pos INT)
RETURNS VARCHAR(255)
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),delim, '');

delimeter //

-- a. Ability to view, add, remove and modify customer information.
CREATE PROCEDURE viewCustomerInfo(in f varchar(16), in l varchar(16))
BEGIN
	SELECT * 
	FROM Customer
	WHERE fname = f AND lname = l;
END//

-- a. Ability to view, add, remove and modify customer information.
CREATE PROCEDURE addCustomerInfo(in first varchar(16), in last 	varchar(16), in newStreet varchar(32), in newCity 	varchar(32), in newZip int(9))
BEGIN
	INSERT INTO Customer(fname, lname, street, city, zip) VALUES(first, last, newStreet, newCity, newZip);
END//

-- a. Ability to view, add, remove and modify customer information.
CREATE PROCEDURE removeCustomer(in id int(32))
BEGIN
	DELETE FROM Customer
	WHERE CID = id;
END//

-- a. Ability to view, add, remove and modify customer information.
CREATE PROCEDURE modifyCustomerInfo(in id int(32), in `first` varchar(16), in `last` varchar(16), in newStreet varchar(32), in newCity varchar(32), in newZip int(9))
BEGIN
	UPDATE Customer SET 
		fname = CASE WHEN `first` IS NOT NULL THEN `first` ELSE fname END,
		lname = CASE WHEN `last` IS NOT NULL THEN `last` ELSE lname END,
		street = CASE WHEN newStreet IS NOT NULL THEN newStreet ELSE street END,
		city = CASE WHEN newCity IS NOT NULL THEN newCity ELSE city END,
		zip = CASE WHEN newZip IS NOT NULL THEN newZip ELSE zip END
	WHERE CID = id;
END//

-- b. Ability to view, add, remove and modify supplier information.
CREATE PROCEDURE viewSupplierInfo(in name varchar(32))
BEGIN
	SELECT *
	FROM Supplier
	WHERE sname = name;
END//

-- b. Ability to view, add, remove and modify supplier information.
CREATE PROCEDURE addSupplier(in name varchar(32), newCity 	varchar(32), newZip int(9))
BEGIN
	INSERT INTO Supplier(sname, city, zip)
	VALUES(name, newCity, newZip);
END//

-- b. Ability to view, add, remove and modify supplier information.
CREATE PROCEDURE removeSupplier(in name varchar(32))
BEGIN
	DELETE FROM Supplier
	WHERE sname = name;
END//

-- b. Ability to view, add, remove and modify supplier information.
CREATE PROCEDURE updateSupplierInfo(in name varchar(32), in newCity varchar(32), in newZip int(9))
BEGIN
	UPDATE Supplier
	SET
		city = CASE WHEN newCity IS NOT NULL THEN newCity ELSE city END,
		zip = CASE WHEN newZip IS NOT NULL THEN newZip ELSE zip END
	WHERE sname = name;
END//

-- c. Ability to view, add, remove and modify product information.
CREATE PROCEDURE view_products(IN searchKey varchar(32))
BEGIN
    SELECT upc, name, price,sname FROM product
    WHERE (searchKey NOT LIKE '%;%') AND ((upc OR name OR sname) LIKE '%'|| searchKey||'%');
END//

-- c. Ability to view, add, remove and modify product information.
CREATE PROCEDURE delete_product(IN delUPC int(32))
BEGIN
    DELETE FROM product WHERE upc=delUPC;
END//

-- c. Ability to view, add, remove and modify product information.
CREATE PROCEDURE modify_product(IN modUPC int(32), IN newPrice decimal(10,2), IN newSup varchar(32), IN newName varchar(32), IN newAmt int(16))
BEGIN
    UPDATE product SET
        name = CASE WHEN newName IS NOT NULL THEN newName ELSE name END,
        sname = CASE WHEN newSup IS NOT NULL THEN newSup ELSE sname END,
        price = CASE WHEN newPrice IS NOT NULL THEN newPrice ELSE price END,
        amount = CASE WHEN newAmt  IS NOT NULL THEN newAmt ELSE amount END
    WHERE upc=modUPC;
END//

-- c. Ability to view, add, remove and modify product information.
CREATE PROCEDURE add_product(IN inupc int(32), IN nam varchar(32), IN sup varchar(32), IN pri decimal(10,2), IN amt int(32), IN relvl int(32), IN catArray varchar(255))
BEGIN
    DECLARE `a` int(32) default 0;
    DECLARE str varchar(255);
    INSERT INTO `product`(`upc`,`name`,`sname`,`price`,`amount`,`reorderlevel`) VALUES (inupc,nam,sup,pri,amt,relvl);
    sim_loop: LOOP
        SET `a`=`a`+1;
        SET str = SPLIT_STR(catArray,',',`a`);
        IF str = '' THEN
            LEAVE sim_loop;
        ELSEIF 3 <= (SELECT count(*) FROM prod_category WHERE upc=inupc) THEN
            LEAVE sim_loop;
        ELSE
            INSERT INTO prod_category(upc, category) VALUES (inupc, str);
        END IF;
    END LOOP;
END//

-- d. Ability to view inventory.
CREATE PROCEDURE view_inventory(IN searchUPC int(32))
BEGIN
    SELECT upc, amount FROM product WHERE upc LIKE '%'||searchUPC||'%';
END//

-- e. Ability to modify inventory (as a result of re-ordering).
CREATE PROCEDURE modify_inventory(IN modUPC int(32))
BEGIN
    UPDATE product
    SET ammount=newAmt
    WHERE upc=modUPC;
END//

-- f. Ability to load information from flat files.
CREATE PROCEDURE load_data(IN filepath varchar(255), IN tablename varchar(32))
BEGIN
    LOAD DATA LOCAL INFILE `filepath` into table `tablename` fields terminated by ',';
END//

-- g. Ability to place an order.
CREATE PROCEDURE placeOrder(in customerID int(32), in payment varchar(16), in CCN int(16), in upcArray varchar(255), in quantityArray varchar(255))
BEGIN
    DECLARE i int default 0;
    DECLARE n int default 1;
    DECLARE curr_oid int(32) default 0;
    INSERT INTO `order`(CID, payment_type, card_number, date_ordered) VALUES (customerID, payment, CCN, CURRENT_DATE);
    SELECT oid INTO curr_oid FROM `order` ORDER BY oid desc LIMIT 1; 
    CREATE TEMPORARY TABLE temp AS SELECT upc FROM product WHERE FIND_IN_SET(upc,upcArray);
    CREATE TEMPORARY TABLE tempq (quantity int(16) NOT NULL);
    WHILE n < (SELECT count(*) FROM temp)
        INSERT INTO tempq(quantity) VALUES SELECT SUBSTRING(quantityArray,2n-1,1);
    END WHILE;
    WHILE i < (SELECT count(*) FROM temp)
        INSERT INTO ordered_product (oid,upc,quantity) VALUES (curr_oid, SELECT upc FROM temp LIMIT 1, SELECT quantity FROM tempq LIMIT 1);
        UPDATE products SET ammount = ammount - (SELECT quantity FROM tempq LIMIT 1) WHERE upc IN (SELECT upc FROM temp LIMIT 1);
        DELETE FROM temp LIMIT 1;
        DELETE FROM tempq LIMIT 1;
    END WHILE;
    DROP TEMPORARY TABLE temp;
    DROP TEMPORARY TABLE tempq;
END//

-- h. Ability to track an order.
CREATE PROCEDURE trackOrder(IN id int(32))
BEGIN
    SELECT distinct oid, date_ordered, date_shipped
    FROM `order`
    WHERE cid = id;
END//    

-- i. Ability to view, add and remove products on the wish list.
CREATE PROCEDURE viewWishList(IN id int(32))
BEGIN
    SELECT UPC, name, price
    FROM Product
    WHERE UPC IN (SELECT UPC FROM WishList WHERE CID = id);
END//

-- i. Ability to view, add and remove products on the wish list.
CREATE PROCEDURE addToWishList(IN customerID int(32), IN productID int(32))
BEGIN
    INSERT INTO WishList(CID, UPC)
    VALUES(customerID, productID);
END//

-- i. Ability to view, add and remove products on the wish list.
CREATE PROCEDURE removeFromWishList(IN customerID int(32), IN productID int(32))
BEGIN
    DELETE FROM WishList
    WHERE CID = customerID AND UPC = productID;
END//

-- j. Ability to rate products.
CREATE PROCEDURE rateProduct(IN customerID int(32), IN productID int(32), IN thisRating int(1))
BEGIN
    INSERT INTO Rating(CID, UPC, rating)
    VALUES(customerID, productID, thisRating);
END//

-- k. List of products whose inventory is at reorder level.
CREATE PROCEDURE getReOrderProducts()
BEGIN
    SELECT *
    FROM Product
    WHERE amount <= reorderlevel;
END//

-- l. Customers who have not been “too active”(*) and for whom special offers should be made.
CREATE PROCEDURE underactive_custs()
BEGIN
    SELECT cid, fname, lname FROM customer WHERE cid IN (SELECT distinct o1.cid FROM `order` o1 WHERE (SELECT count(*) FROM `order` WHERE cid=o1.cid) <=3);
END//

-- m. List of products that are not selling “too well”(*), which might be offered as specials.
CREATE PROCEDURE prods_not_selling()
BEGIN
    SELECT upc, name FROM product WHERE upc IN (SELECT distinct upc FROM ordered_product o1 WHERE (SELECT count(*) FROM ordered_product WHERE upc=o1.upc) <= 4);
END//

-- n. List of customers who have rated products that they did not buy.
CREATE PROCEDURE getNonPurchasedReviews(IN customerID int(32))
BEGIN
    SELECT UPC
    FROM Rating
    WHERE CID = customerID AND UPC NOT IN(SELECT UPC FROM ordered_product WHERE oid IN (SELECT oid FROM `order` WHERE cid = customerID));
END//

-- o. List of highly rated products.
CREATE PROCEDURE getHotItems()
BEGIN
    CREATE TEMPORARY TABLE avg_rating AS SELECT upc, AVG(rating) as avrat FROM rating GROUP BY upc;
    SELECT UPC, name
    FROM Product
    WHERE UPC IN (SELECT distinct r.UPC FROM rating r WHERE 4.00 <= (SELECT avrat FROM avg_rating WHERE upc=r.upc) GROUP BY upc);
END//

-- p. List of highly wished products.
CREATE PROCEDURE getWishedFor()
BEGIN
    SELECT upc, name FROM product p LEFT JOIN (SELECT upc, count(cid) as tot FROM wishlist GROUP BY upc) w USING(upc) WHERE tot >= 10 ORDER BY tot desc;
END//

-- q. List of wished products that have never been bought by the customers who wish them.
CREATE PROCEDURE wishedNotBought()
BEGIN
    SELECT p.upc, p.name, w.cid FROM wishlist w LEFT JOIN product p USING(upc)
    WHERE (p.upc,w.cid) NOT IN (SELECT o.upc,c.cid FROM ordered_product o LEFT JOIN `order` c using(oid));
END//

-- r. List of customers who did not rate any products they bought.
CREATE PROCEDURE bought_not_rated()
BEGIN
    SELECT cid, fname, lname FROM customer c WHERE cid IN (SELECT cid FROM `order` o WHERE oid IN (SELECT oid FROM ordered_product WHERE upc NOT IN (SELECT upc FROM rating WHERE cid=o.cid)));
END//

-- s. List of bestselling products.
CREATE PROCEDURE getBestSellingProducts(IN customerID int(32))
BEGIN
    SELECT upc, name, price
    FROM product LEFT JOIN (SELECT oid, upc, count(oid) as sold FROM ordered_product) op USING(upc)
    WHERE oid IN (SELECT oid FROM `order` WHERE cid=customerID) ORDER BY sold desc LIMIT 10;
END//


-- t. A suggested list of products for each customer.
CREATE PROCEDURE get_suggested_products(IN custid int(32))
BEGIN
    CREATE TEMPORARY TABLE cust_cat AS 
        SELECT category FROM  prod_category
        WHERE upc IN (SELECT upc FROM ordered_product WHERE oid IN(SELECT oid FROM `order` WHERE cid=custid)) ORDER BY COUNT(*);
    CREATE TEMPORARY TABLE avg_rating AS
        SELECT upc, AVG(rating) as avrat FROM rating
        GROUP by upc;
    SELECT upc, p.name, p.price, a.avrat FROM product p LEFT JOIN avg_rating a USING(upc)
    WHERE upc IN (SELECT distinct upc FROM prod_category WHERE category IN (SELECT * FROM cust_cat))
        AND upc NOT IN (SELECT distinct upc FROM ordered_product WHERE oid IN (SELECT oid FROM `order` WHERE cid=custid)) ORDER BY avrat desc;
    DROP TEMPORARY TABLE cust_cat;
    DROP TEMPORARY TABLE avg_rating;
END//

-- v. Find and generate interesting reports (other than the ones above) that would help MyFancyRetailer promote their business or make any changes you think might be beneficial. Explain why they are better.
CREATE PROCEDURE cust_total_spending()
BEGIN
    SELECT o.cid, SUM(p.price) AS total_spending FROM (SELECT cid FROM customer) c RIGHT JOIN (SELECT cid, oid FROM `order`) o RIGHT JOIN ordered_product op USING(oid) LEFT JOIN (SELECT upc, price FROM product) p USING(upc)
    GROUP BY o.cid ORDER BY total_spending desc;
END//

CREATE PROCEDURE get_best_custs()
BEGIN
    SELECT o.cid, SUM(p.price) AS total_spending FROM (SELECT cid FROM customer) c RIGHT JOIN (SELECT cid, oid FROM `order`) o RIGHT JOIN ordered_product op USING(oid) LEFT JOIN (SELECT upc, price FROM product) p USING(upc)
    GROUP BY o.cid ORDER BY total_spending desc LIMIT 10;
END//

delimeter ;
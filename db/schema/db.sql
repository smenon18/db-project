-- db-project schema
-- Author: Shaan Menon
-- Date: 10th April 2016

DROP TABLE IF EXISTS supplier;
CREATE TABLE supplier(
    `sname` varchar(32) PRIMARY KEY,
    `city` varchar(32) NOT NULL,
    `zip` int(9) NOT NULL
);

DROP TABLE IF EXISTS customer;
CREATE TABLE customer(
    `cid` int(32) PRIMARY KEY AUTO_INCREMENT,
    `fname` varchar(16) NOT NULL,
    `lname` varchar(16) NOT NULL,
    `street` varchar(32) NOT NULL,
    `city` varchar(32) NOT NULL,
    `state` varchar(32) NOT NULL,
    `zip` int(9) NOT NULL,
    `username` varchar(32) UNIQUE NOT NULL,
    `password` varchar(64) NOT NULL
);

DROP TABLE IF EXISTS product;
CREATE TABLE product(
    `upc` int(32) PRIMARY KEY,
    `name` varchar(32) NOT NULL,
    `price` decimal(10,2) NOT NULL,
    `sname` varchar(32) NOT NULL,
    `amount` int(16) NOT NULL,
    `reorderlevel` int(16) NOT NULL DEFAULT 10,
    FOREIGN KEY(sname) REFERENCES supplier(sname) ON DELETE CASCADE
);

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`(
    oid int(32) PRIMARY KEY AUTO_INCREMENT,
    payment_type varchar(16) NOT NULL,
    card_number int(32),
    cid varchar(32) NOT NULL,
    date_ordered date NOT NULL,
    date_shipped date,
    FOREIGN KEY(cid) REFERENCES customer(cid) ON DELETE CASCADE
);

DROP TABLE IF EXISTS ordered_product;
CREATE TABLE ordered_product(
    oid int(32) NOT NULL,
    upc int(32) NOT NULL,
    quantity int(16) NOT NULL,
    PRIMARY KEY(oid,upc),
    FOREIGN KEY(oid) REFERENCES `order`(oid) ON DELETE CASCADE,
    FOREIGN KEY(upc) REFERENCES product(upc) ON DELETE CASCADE
);

DROP TABLE IF EXISTS wishlist;
CREATE TABLE wishlist(
    cid int(32) NOT NULL,
    upc int(32) NOT NULL,
    PRIMARY KEY(cid,upc),
    FOREIGN KEY(cid) REFERENCES customer(cid) ON DELETE CASCADE,
    FOREIGN KEY(upc) REFERENCES product(upc) ON DELETE CASCADE
);

DROP TABLE IF EXISTS rating;
CREATE TABLE rating(
    cid int(32) NOT NULL,
    upc int(32) NOT NULL,
    rating int(1) NOT NULL DEFAULT 1,
    rating_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(cid) REFERENCES customer(cid),
    FOREIGN KEY(upc) REFERENCES product(upc) ON DELETE CASCADE
);

DROP TABLE IF EXISTS prod_category; 
CREATE TABLE prod_category(
    upc int(32) NOT NULL,
    category varchar(16) NOT NULL,
    PRIMARY KEY(upc,category),
    FOREIGN KEY(upc) REFERENCES product(upc) ON DELETE CASCADE
);

DROP TABLE IF EXISTS `user`;
CREATE TABLE user(
    `uid` int(32) PRIMARY KEY AUTO_INCREMENT,
    `username` varchar(32) UNIQUE NOT NULL,
    `password` varchar(64) NOT NULL,
    `access_level` int(1) NOT NULL
);

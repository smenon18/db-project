<?php
    require "connect.php";
    
    $upc = $_POST["upc"];
    $sql = "SELECT * FROM rating LEFT JOIN (SELECT cid, fname, lname FROM customer GROUP BY cid) cus ON (rating.cid=cus.cid) WHERE upc=" . $upc;
    
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $ratings = array();
        
        for($i = 0; $i < sizeof($row); $i++)
        {
            $rating = new stdClass;
            $rating->rating = $row[$i]["rating"];
            $rating->ratingDate = $row[$i]["rating_date"];
            $rating->firstName = $row[$i]["fname"];
            $rating->lastName = $row[$i]["lname"];
            
            array_push($ratings, $rating);
        }
        
        echo json_encode($ratings);
    }
    else
    {
        $success = false;
        echo json_encode($succss);
    }
?>


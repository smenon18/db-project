<?php
    require "connect.php";
    
    $id = $_POST["cid"];
    $sql = "CALL get_suggested_products('$id')";
    $result = $connect->query($sql);
    
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $suggestedProducts = array();
        
        for($i = 0; $i < sizeof($row); $i++)
        {
            $suggestedProduct = new stdClass;
            $suggestedProduct->upc = $row[$i]["upc"];
            $suggestedProduct->name = $row[$i]["name"];
            $suggestedProduct->price = $row[$i]["price"];
            
            array_push($suggestedProducts, $suggestedProduct);
        }
        
        echo json_encode($suggestedProducts);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
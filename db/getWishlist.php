<?php
    require "connect.php";
    
    $cid = $_POST["cid"];
    $sql = "CALL viewWishlist('$cid')";
    
    $result = $connect->query($sql);
    if($result->num_rows > 0)
    {
        $row = mysqli_fetch_all ($result, MYSQLI_ASSOC);
        $wishes = array();
        for($i = 0; $i < sizeof($row); $i++)
        {
            $wish = new stdClass;
            $wish->upc = $row[$i]["UPC"];
            $wish->name = $row[$i]["name"];
            $wish->price = $row[$i]["price"];
            
            array_push($wishes, $wish);
        }
        echo json_encode($wishes);
    }
    else
    {
        $success = false;
        echo json_encode($success);
    }
?>
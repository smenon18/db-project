/**
 * Place order for products.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */

function placeOrder()
{
    var cardType = document.getElementById("card").value;
    var cardNumber = document.getElementById("cardNumber").value;
    var cart = JSON.parse(sessionStorage.cart);
    var upcs = "";
    var quantity = "";
    var customerID = sessionStorage.id;
    
    for(var i = 0; i < cart.length; i++)
    {
        upcs += cart[i].id + ",";
        quantity += cart[i].amount + ",";
    }
    
    $.post("../db/placeOrder.php", {type: cardType, number: cardNumber, upcs: upcs, quantity: quantity, cid: customerID}, function(data)
    {
        data = JSON.parse(data);
        console.log(data);
        
        if (data === true)
        {
            alert("Order placed!");
            
            sessionStorage.cart = "";
            cart = new Array();
            sessionStorage.cart = JSON.stringify(cart);
            
            window.location = "../core/myAccount.php";
        }
        else
        {
            alert("Order could not be placed.");
        }
    });
}

var button = document.getElementById("order");
if (button.addEventListener)
{
    button.addEventListener("click", placeOrder, false);
}
else
{
    button.attachEvent("onclick", placeOrder);
}
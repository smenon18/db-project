/**
 * Gets customer activity.
 */

$.post("../db/getActivity.php", function(data)
{
   data = JSON.parse(data);
   if (data === false)
   {
        alert("Could not get customer data.");
   }
   else
   {
        var table = document.getElementById("customers");
        for(var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            
            var cid = document.createElement("td");
            cid.innerHTML = data[i].cid;
            row.appendChild(cid);
            
            var first = document.createElement("td");
            first.innerHTML = data[i].first;
            row.appendChild(first);
            
            var last = document.createElement("td");
            last.innerHTML = data[i].last;
            row.appendChild(last);
            
            table.appendChild(row);
        }
   }
});
/**
 * Redirects to proper pages through my account.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

function goToCheckout()
{
    window.location = "../core/checkout.php";
}

function goToTrackOrders()
{
    window.location = "../core/trackOrders.php";
}

function goToWishList()
{
    window.location = "../core/myWishList.php";
}

var checkout = document.getElementById("checkout");
var track = document.getElementById("track");
var wishlist = document.getElementById("wishlist");

if (checkout.addEventListener || track.addEventListener || wishlist.addEventListener)
{
    checkout.addEventListener("click", goToCheckout, false);
    track.addEventListener("click", goToTrackOrders, false);
    wishlist.addEventListener("click", goToWishList, false);
}
else
{
    checkout.attachEvent("onclick", goToCheckout);
    track.attachEvent("onclick", goToTrackOrders);
    wishlist.attachEvent("onclick", goToWishList);
}
/**
 * Gets the customers wish list.
 * 
 * @author Richard Cerone
 * @date 04/25/2016
 */

var cid = sessionStorage.id;
$.post("../db/getWishlist.php", {cid: cid}, function(data)
{
    data = JSON.parse(data);
    
    if (data === false)
    {
        var message = document.getElementById("wishlist");
        message.innerHTML = "<h4>You have no items in your wishlist</h4>";
    }
    else
    {
        var table = document.getElementById("wishlist");
        for(var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            
            var upc = document.createElement("td");
            upc.innerHTML = data[i].upc;
            row.appendChild(upc);
            
            var name = document.createElement("td");
            name.innerHTML = data[i].name;
            row.appendChild(name);
            
            var price = document.createElement("td");
            price.innerHTML = "$" + data[i].price;
            row.appendChild(price);
            
            table.appendChild(row);
        }
    }
});
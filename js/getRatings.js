/**
 * Gets rating of the product.
 *
 * @autohr Richard Cerone
 * @date 04/22/2016
 */

$.post("../db/getRatings.php", {upc: id}, function(data)
{
    data = JSON.parse(data);
    
    if (data === false)
    {
        var message = document.getElementById("status");
        message.innerHTML = "There are no ratings for this product yet.";
    }
    else
    {
        var ratings = document.getElementById("ratings");
        
        for(var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            
            var firstName = document.createElement("td");
            firstName.innerHTML = data[i].firstName;
            row.appendChild(firstName);
            
            var lastName = document.createElement("td");
            lastName.innerHTML = data[i].lastName;
            row.appendChild(lastName);
            
            var rating = document.createElement("td");
            rating.innerHTML = data[i].rating;
            row.appendChild(rating);
            
            var ratingDate = document.createElement("td");
            ratingDate.innerHTML = data[i].ratingDate;
            row.appendChild(ratingDate);
            
            ratings.appendChild(row);
        }
    }
});
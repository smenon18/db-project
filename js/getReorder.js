/**
 * Gets reorder level for inventory.
 */

$.post("../db/getReorder.php", function(data)
{
    data = JSON.parse(data);
    
    if (data === false)
    {
        alert("Could not get reorder level.");
    }
    else
    {
        var table = document.getElementById("inventory");
        for(var i = 0; i < data.length; i++)
        {
             var row = document.createElement("tr");
             
             var upc = document.createElement("td");
             upc.innerHTML = data[i].upc;
             row.appendChild(upc);
             
             var name = document.createElement("td");
             name.innerHTML = data[i].name;
             row.appendChild(name);
             
             var price = document.createElement("td");
             price.innerHTML = "$" + data[i].price;
             row.appendChild(price);
             
             var supplier = document.createElement("td");
             supplier.innerHTML = data[i].supplier;
             row.appendChild(supplier);
             
             var amount = document.createElement("td");
             amount.innerHTML = data[i].amount;
             row.appendChild(amount);
             
             var reorderlevel = document.createElement("td");
             reorderlevel.innerHTML = data[i].reorderlevel;
             row.appendChild(reorderlevel);
             
             table.appendChild(row);
        }
    }
});
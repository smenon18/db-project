/**
 * Adds item to cart.
 * 
 * @author Richard Cerone
 * @date 04/22/2016
 */

function addToCart()
{
    var upc = getURLValue("id");
    
    $.post("../db/getProductInfo.php", {upc: upc}, function(data)
    {
        data = JSON.parse(data);
        if (data === false)
        {
            alert("Failed to get product info.");
        }
        else
        {
            var product =
            {
                id: upc,
                name: data[0],
                price: data[2],
                amount: document.getElementById("amount").value
            };
            
            var cart = JSON.parse(sessionStorage.cart);
            cart.push(product);
            sessionStorage.cart = JSON.stringify(cart);
            
            alert(data[0] + " added to cart!");
        }
    });
}

var button = document.getElementById("add");
if (button.addEventListener)
{
    button.addEventListener("click", addToCart, false);
}
else
{
    button.attachEvent("onclick", addToCart);
}
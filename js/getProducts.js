/**
 * Gets all products.
 *
 * @author Richard Cerone
 * @date 04/22/2016
 */

$.post("../db/getProducts.php", function(data)
{
    data = JSON.parse(data);
    if (data === false)
    {
        alert("Failed to load product data.");
    }
    else
    {
        var product = document.getElementById("product-table");
        for (var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            row.id = data[i].upc;
            
            var td1 = document.createElement("td");
            td1.innerHTML = "<a href='../core/productPage.php?id=" + data[i].upc + "'>" + data[i].name + "</a>";
            row.appendChild(td1);
            
            var td2 = document.createElement("td");
            td2.innerHTML = data[i].supplier;
            row.appendChild(td2);
            
            var td3 = document.createElement("td");
            td3.innerHTML = data[i].price;
            row.appendChild(td3);
            
            product.appendChild(row);
        }
    }
});
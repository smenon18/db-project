/**
 * Gets the info of the product on the product page.
 *
 * @author Richard Cerone
 * @date 04/23/2016
 */

var id = getURLValue("id");

if (id === false)
{
    alert("Sorry, there is no info on this product.");
}
else
{
    $.post("../db/getProductInfo.php", {upc: id}, function(data)
    {
        data = JSON.parse(data);
        
        if (data === false)
        {
            alert("Failed to get product info.");
        }
        else
        {
            var name = document.getElementById("name");
            name.innerHTML = data[0];
            
            var supplier = document.getElementById("supplier");
            supplier.innerHTML = "Supplied By: " + data[1];
            
            var price = document.getElementById("price");
            price.innerHTML = "Only $" + data[2];
        }
    });
}

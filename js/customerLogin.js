/**
 * Main page where customer logs in.
 *
 * @author Richard Cerone
 * @date 04/21/2016
 */

function login(args)
{
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    if (username !== "" || password !== "")
    {
        $.post("../db/customerLogin.php", {username: username, password: password}, function(data)
        {
            data = JSON.parse(data);
            
            if (data === false)
            {
                 alert("You entered the wrong username or password. Please try again.");   
            }
            else
            {
                sessionStorage.username = data.username;
                sessionStorage.firstName = data.firstName;
                sessionStorage.lastName = data.lastName;
                sessionStorage.state = data.state;
                sessionStorage.address = data.street;
                sessionStorage.zip = data.zip;
                sessionStorage.city = data.city;
                sessionStorage.id = data.id;
                sessionStorage.loggedIn = true;
                var cart = new Array();
                sessionStorage.cart = JSON.stringify(cart);
                
                window.location = "../core/myAccount.php"; //Go to account page.
            }
        });
    }
}

var button = document.getElementById("login");
if (button.addEventListener)
{
    button.addEventListener("click", login, false);
}
else
{
    button.attachEvent("onclick", login);
}
/**
 * Allows admin login.
 */

function login()
{
    var username = document.getElementById("adminName").value;
    var password = document.getElementById("adminPassword").value;
    
    $.post("../db/adminLogin.php", {username: username, password: password}, function(data)
    {
        data = JSON.parse(data);
        if (data === true)
        {
            window.location = "../core/adminPanel.php";
        }
        else
        {
            alert("Wrong login info.");
        }
    });
}

var button = document.getElementById("adminLogin");
if (button.addEventListener)
{
    button.addEventListener("click", login, false);
}
else
{
    button.addEventListener("onclick", login);
}
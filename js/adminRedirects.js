/**
 * Redirects to proper pages in admin panel.
 */

function inventory()
{
    window.location = "../core/inventory.php";
}

function activity()
{
    window.location = "../core/activity.php";
}

function wishlist()
{
    window.location = "../core/wishlists.php";
}

var inv = document.getElementById("inventory");
var act = document.getElementById("activity");
var wish = document.getElementById("wishlist");

if (inv.addEventListener || act.addEventListener || wish.addEventListener)
{
    inv.addEventListener("click", inventory, false);
    act.addEventListener("click", activity, false);
    wish.addEventListener("click", wishlist, false);
}
else
{
    inv.attachEvent("onclick", inventory);
    act.attachEvent("onclick", activity);
    wish.attachEvent("onclick", wishlist);
}
/**
 * Will redirect user to my account instead of login screen if logged in each time they click MyFancyRetailer logo.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

if (sessionStorage.loggedIn)
{
    window.location = "../core/myAccount.php";
}
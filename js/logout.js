/**
 * Lets the customer logout.
 *
 * @author Richard Cerone
 * @date 04/22/2016
 */

function logout()
{
    sessionStorage.clear();
    alert("You've been logged out.");
    window.location = "../core/index.php";
}

var button = document.getElementById("logout");
if (button.addEventListener)
{
    button.addEventListener("click", logout, false);
}
else
{
    button.attachEvent("onclick", logout);
}
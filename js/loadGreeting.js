/**
 * Simply inserts username into welcome message upon myAccount page load.
 *
 * @author Richard Cerone
 * @date 04/22/2016
 */

var message = document.getElementById("greeting");
message.innerHTML += " " + sessionStorage.firstName + " " + sessionStorage.lastName;

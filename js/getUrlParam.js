/**
 * Gets parameters passed through the url.
 *
 * @author Richard Cerone
 * @date 04/23/2016
 */

/**
 *
 * @param {String} value the value is the variable in the url.
 * @returns {getURLValue.keyValuePair|Boolean} If we find the string we will
 * return the value for the corresponding variable. If not we return false.
 */
function getURLValue(value)
{
    //Get rid of the ? in the url string.
    var query = window.location.search.substring(1);
    //This will split & and put multiple values into an array if more than one value.
    var queryArray = query.split('&');
    //Search for the value we want.
    for(var i = 0; i < queryArray.length; i++)
    {
        //separate the = from the values and store them in another array.
        var keyValuePair = queryArray[i].split('=');

        /*Note that every time we add a value to the keyValuePair array
         * it is being added at element 0.
         */
        if(keyValuePair[0] === value)
        {
            return keyValuePair[1]; //Now return the assinged value next to it.
        }
    }

    return false; //value not found.
}

/**
 * Add product to wishlist.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

function addToWishlist()
{
    if (sessionStorage.id)
    {
        var productID = getURLValue("id");
        var cid = sessionStorage.id;
        
        $.post("../db/addToWishlist.php", {cid: cid, productID: productID}, function(data)
        {
            data = JSON.parse(data);
            if (data === true)
            {
                alert("Item added to your wishlist!");
            }
            else
            {
                alert("Could not add item to wishlist");
            }
        });
    }
    else
    {
        alert("You need to be logged in to do that.");
    }
}

var wish = document.getElementById("wishlist");
if (wish.addEventListener)
{
    wish.addEventListener("click", addToWishlist, false);
}
else
{
    wish.attachEvent("onclick", addToWishlist);
}
/**
 * Get most wished for items.
 */

$.post("../db/getMostWishedFor.php", function(data)
{
    data = JSON.parse(data);
    
    if (data === false)
    {
        alert("Could not get Wishes");
    }
    else
    {
        var table = document.getElementById("items");
        for(var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            
            var upc = document.createElement("td");
            upc.innerHTML = data[i].upc;
            row.appendChild(upc);
            
            var name = document.createElement("td");
            name.innerHTML = data[i].name;
            row.appendChild(name);
            
            table.appendChild(row);
        }
    }
});
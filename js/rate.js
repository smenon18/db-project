/**
 * Rates a product.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

function rate()
{
    if (sessionStorage.loggedIn)
    {
        var rating = document.getElementById("score").value;
        var cid = sessionStorage.id;
        var productID = getURLValue("id");
        $.post("../db/rateProduct.php", {score: rating, cid: cid, productID: productID}, function(data)
        {
            data = JSON.parse(data);
            if (data === true)
            {
                alert("Product Rated!");
                window.location = "../core/productPage.php?id=" + productID;
            }
            else
            {
                alert("Could not rate product.");
            }
        });
    }
    else
    {
        alert("You need to be logged in to do that.");
    }
}

var rateThis = document.getElementById("rate");
if (rateThis.addEventListener)
{
    rateThis.addEventListener("click", rate, false);
}
else
{
    rateThis.attachEvent("onclick", rate);
}
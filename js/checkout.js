/**
 * Goes to the place order page.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */

function checkout()
{
    window.location = "../core/placeOrder.php";
}

var button = document.getElementById("checkout");
if (button.addEventListener)
{
    button.addEventListener("click", checkout, false);
}
else
{
    button.attachEvent("onclick", checkout);
}
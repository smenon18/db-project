/**
 * Cancels the order and goes back to checkout.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */

function cancelOrder()
{
    window.location = "../core/checkout.php";
}

var button = document.getElementById("cancel");
if (button.addEventListener)
{
    button.addEventListener("click", cancelOrder, false);
}
else
{
    button.attachEvent("onclick", cancelOrder);
}
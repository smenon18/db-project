/**
 * Loads items into cart table.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */

var cart = JSON.parse(sessionStorage.cart);
var table = document.getElementById("cart");

if (cart.length > 0)
{
    var totalPrice = Number(0);
    for(var i = 0; i < cart.length; i++)
    {
        var row = document.createElement("tr");
        row.id = cart[i].id;
        
        var productName = document.createElement("td");
        productName.innerHTML = "<a href='productPage.php?id=" + cart[i].id + "'>" + cart[i].name + "</a>";
        row.appendChild(productName);
        
        var price = document.createElement("td");
        price.innerHTML = cart[i].price;
        row.appendChild(price);
        
        var amount = document.createElement("td");
        amount.innerHTML = cart[i].amount;
        row.appendChild(amount);
        
        table.appendChild(row);
        totalPrice += Number(cart[i].price * cart[i].amount);
    }
    
    var total = document.getElementById("total");
    total.innerHTML += " " + totalPrice;
}
else
{
    table.innerHTML = "<h4 align='center'>Your cart is empty.</h4>"
}
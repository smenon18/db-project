/**
 * Calculates the average rating.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

$.post("../db/calculateAverageRating.php", {upc: id}, function(data)
{
    data = JSON.parse(data);
    var avgRating = document.getElementById("avgRating");
     
    if (data === false)
    {
        avgRating.innerHTML += " No ratings yet."
    }
    else
    {
        avgRating.innerHTML += " " + data[0].rating;    
    }
});

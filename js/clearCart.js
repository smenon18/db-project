/**
 * Clears the cart.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */

function clearCart()
{
    var cart = new Array();
    sessionStorage.cart = "";
    sessionStorage.cart = JSON.stringify(cart);
    
    alert("Cart has been emptied.");
    window.location = "../core/checkout.php";
}

var button = document.getElementById("clear");
if (button.addEventListener)
{
    button.addEventListener("click", clearCart, false);
}
else
{
    button.attachEvent("onclick", clearCart);
}
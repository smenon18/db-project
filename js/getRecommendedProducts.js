/**
 * Get recommended products for customer.
 *
 * @author Richard Cerone
 * @date 04/23/2015
 */

function loggedIn()
{
    if (sessionStorage.loggedIn)
    {
        return true;
    }
    else
    {
        return false;
    }
}

if (loggedIn())
{
    var id = sessionStorage.id;
    $.post("../db/getRecommendedProducts.php", {cid: id}, function(data)
    {
        data = JSON.parse(data);
        
        if (data === false)
        {
            alert("You don't have any suggested products");
        }
        else
        {
            var title = document.getElementById("suggested");
            title.innerHTML = "<h4>Suggested For " + sessionStorage.firstName + " " + sessionStorage.lastName + "</h4>";
            
            var suggested = document.getElementById("suggested-product-table");
            
            var headerRow = document.createElement("tr");
            
            var header1 = document.createElement("th");
            header1.innerHTML = "Product";
            headerRow.appendChild(header1);
            
            var header2 = document.createElement("th");
            header2.innerHTML = "Price";
            headerRow.appendChild(header2);
            
            suggested.appendChild(headerRow);
            
            for(var i = 0; i < data.length; i++)
            {
                var row = document.createElement("tr");
                
                var td1 = document.createElement("td");
                td1.innerHTML = "<a href='productPage.php?id=" + data[i]["upc"] + "'>" + data[i]["name"];
                row.appendChild(td1);
                
                var td2 = document.createElement("td");
                td2.innerHTML = data[i]["price"];
                row.appendChild(td2);
                
                suggested.appendChild(row);
            }
        }
    });
}
/**
 * Signs up customer.
 *
 * @author Richard Cerone
 * @date 04/24/2016
 */

function signup()
{
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var state = document.getElementById("state").value;
    var address = document.getElementById("street").value;
    var city = document.getElementById("city").value;
    var zip = document.getElementById("zip").value;
    
    if (firstName === "" || lastName === "" || username === "" || password === "" || state === "" || address === "" || city === "" || zip === "")
    {
        alert("You left a field blank.");
    }
    else
    {
        $.post("../db/signup.php", {firstName: firstName, lastName: lastName, username: username, password: password, state: state, address: address, city: city, zip: zip},
        function(data)
        {
            data = JSON.parse(data);
            
            if (data !== false)
            {
                alert("Account created!");
                
                sessionStorage.username = username;
                sessionStorage.firstName = firstName;
                sessionStorage.lastName = lastName;
                sessionStorage.state = state;
                sessionStorage.address = address;
                sessionStorage.zip = zip;
                sessionStorage.city = city;
                sessionStorage.id = data[1].cid;
                sessionStorage.loggedIn = true;
                var cart = new Array();
                sessionStorage.cart = JSON.stringify(cart);
                
                window.location = "../core/myAccount.php";
            }
            else
            {
                alert("Failed to create account it may already exist");
            }
        });
    }
}

var button = document.getElementById("signup");
if (button.addEventListener)
{
    button.addEventListener("click", signup, false);
}
else
{
    button.attachEvent("onclick", signup);
}
/**
 * Check if a user is logged in.
 *
 * @author Richard Cerone
 * @date 04/22/2016
 */


if (!sessionStorage.username)
{
    window.location = "../core/index.php"; //Redirect to index page.
}

/**
 * Gets tracking info.
 *
 * @author Richard Cerone
 * @date 04/25/2016
 */


var cid = sessionStorage.id;
$.post("../db/getOrders.php", {cid: cid}, function(data)
{
    data = JSON.parse(data);
    
    if (data === false)
    {
        alert("You have no orders being tracked");
    }
    else
    {
        var table = document.getElementById("orders");
        
        for(var i = 0; i < data.length; i++)
        {
            var row = document.createElement("tr");
            
            var oid = document.createElement("td");
            oid.innerHTML = data[i].orderID;
            row.appendChild(oid);
            
            var dateOrdered = document.createElement("td");
            dateOrdered.innerHTML = data[i].dateOrdered;
            row.appendChild(dateOrdered);
            
            var dateShipped = document.createElement("td");
            if (data[i].dateShipped === "")
            {
                dateShipped.innerHTML = "Not Shipped Yet";
            }
            else
            {
                dateShipped.innerHTML = data[i].dateShipped;
            }
            row.appendChild(dateShipped);
            
            table.appendChild(row);
        }
    }
});
